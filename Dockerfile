FROM python:3.6.5

RUN pip install influxdb paho-mqtt

ADD logger.py /

CMD ["python","./logger.py"]

