import struct
import os
import paho.mqtt.client as mqtt
import sys
from datetime import datetime
import base64
from influxdb import InfluxDBClient
import json


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
	print("Connected with result code " + str(rc))
	client.subscribe("$SYS/#")

# The callback for when a PUBLISH message is received from the server.

def dev_name_from_dev_eui(dev_eui):
	retstr='cern-charm-'
	return retstr+str(int(dev_eui)%100)

def binaryToFloat(value):
	hx = hex(int(value, 2))
	print(hx)
	return struct.unpack("d", struct.pack("q", int(hx, 16)))[0]
	
def on_message(client, userdata, msg):
	start_txt_cmp = "payload_raw"
	end_txt_cmp = "metadata"
	activ_txt_cmp = "app_eui"
	msg = (msg.payload).decode("utf-8")
	json_decoded = json.loads(msg)
	if(msg.find(activ_txt_cmp) >= 0):
		json_body = []
		complete_dict = {}
		complete_dict['measurement'] = "resets"
		complete_dict['tags'] = {}
		complete_dict['tags']["dev_eui"] = dev_name_from_dev_eui(json_decoded["dev_eui"])
		payload_dict = {}
		payload_dict['reset'] = 1
		complete_dict['fields'] = payload_dict
		json_body.append(complete_dict)
		influx_client.write_points(json_body)
	else:
		start_position = (msg.find(start_txt_cmp)) + 14
		end_position = (msg.find(end_txt_cmp)) - 3

		payload_raw = msg[start_position:end_position]

		decoded = base64.decodebytes(payload_raw.encode())
		payload_decoded = "".join(["{:08b}".format(x) for x in decoded])
		if (decoded[0:2]==b'v2'):
			try:
				packet_counter = int(payload_decoded[32:64], 2)
				adc_reading_binary = payload_decoded[80:144]
				adc_reading=binaryToFloat(adc_reading_binary)
			except:
				packet_counter = -1
				adc_reading = -1

			json_body = []
			complete_dict = {}
			complete_dict['measurement'] = "lora_read_adc"
			complete_dict['tags'] = {}
			complete_dict['tags']["device"] = json_decoded["dev_id"]
			complete_dict['tags']["application"] = json_decoded["app_id"]
			payload_dict = {}
			payload_dict['packet_counter'] = packet_counter
			payload_dict['adc_reading'] = adc_reading
			complete_dict['fields'] = payload_dict
			json_body.append(complete_dict)
		elif (decoded[0:2]==b'v3'):
			try:
				packet_counter = int(payload_decoded[32:64], 2)
				adc_reading_binary = payload_decoded[80:144]
				adc_reading=binaryToFloat(adc_reading_binary)
				seu_adc_low = int(payload_decoded[160:192], 2)
				seu_adc_high = int(payload_decoded[208:240], 2)
			except:
				packet_counter = -1
				adc_reading = -1
				seu_adc_low = -1
				seu_adc_high = -1

			json_body = []
			complete_dict = {}
			complete_dict['measurement'] = "lora_read_adc_seu"
			complete_dict['tags'] = {}
			complete_dict['tags']["device"] = json_decoded["dev_id"]
			complete_dict['tags']["application"] = json_decoded["app_id"]
			payload_dict = {}
			payload_dict['packet_counter'] = packet_counter
			payload_dict['adc_reading'] = adc_reading
			payload_dict['seu_adc_low'] = seu_adc_low
			payload_dict['seu_adc_high'] = seu_adc_high
			complete_dict['fields'] = payload_dict
			json_body.append(complete_dict)
		elif (decoded[0:2]==b'v4'):
			try:
				packet_counter = int(payload_decoded[32:64], 2)
				seu0_90nm = int(payload_decoded[72:104], 2)
				seu1_90nm = int(payload_decoded[120:152], 2)
				seu2_90nm = int(payload_decoded[168:200], 2)
				seu0_65nm = int(payload_decoded[240:272], 2)
				seu1_65nm = int(payload_decoded[288:320], 2)
				seu2_65nm = int(payload_decoded[336:368], 2)
			except:
				packet_counter = -1
				seu0_90nm = -1
				seu1_90nm = -1
				seu2_90nm = -1
				seu0_65nm = -1
				seu1_65nm = -1
				seu2_65nm = -1

			json_body = []
			complete_dict = {}
			complete_dict['measurement'] = "batmon_3seu_read"
			complete_dict['tags'] = {}
			complete_dict['tags']["device"] = json_decoded["dev_id"]
			complete_dict['tags']["application"] = json_decoded["app_id"]
			payload_dict = {}
			payload_dict['packet_count'] = packet_counter
			payload_dict['seu0_65nm'] = seu0_65nm
			payload_dict['seu1_65nm'] = seu1_65nm
			payload_dict['seu2_65nm'] = seu2_65nm
			payload_dict['seu0_90nm'] = seu0_90nm
			payload_dict['seu1_90nm'] = seu1_90nm
			payload_dict['seu2_90nm'] = seu2_90nm
			complete_dict['fields'] = payload_dict
			json_body.append(complete_dict)
		else:
			try:
				packet_counter = int(payload_decoded[0:32], 2)
				errors_90nm = int(payload_decoded[72:104], 2)
				seu_90nm = int(payload_decoded[120:152], 2)
				mbu_90nm = int(payload_decoded[168:200], 2)
				errors_65nm = int(payload_decoded[240:272], 2)
				seu_65nm = int(payload_decoded[288:320], 2)
				mbu_65nm = int(payload_decoded[336:368], 2)
			except:
				packet_counter = -1
				errors_90nm = -1
				seu_90nm = -1
				mbu_90nm = -1
				errors_65nm = -1
				seu_65nm = -1
				mbu_65nm = -1

			json_body = []
			complete_dict = {}
			complete_dict['measurement'] = "batmon_read"
			complete_dict['tags'] = {}
			complete_dict['tags']["device"] = json_decoded["dev_id"]
			complete_dict['tags']["application"] = json_decoded["app_id"]
			payload_dict = {}
			payload_dict['packet_counter'] = packet_counter
			payload_dict['errors_65nm'] = errors_65nm
			payload_dict['seu_65nm'] = seu_65nm
			payload_dict['mbu_65nm'] = mbu_65nm
			payload_dict['errors_90nm'] = errors_90nm
			payload_dict['seu_90nm'] = seu_90nm
			payload_dict['mbu_90nm'] = mbu_90nm
			complete_dict['fields'] = payload_dict
			json_body.append(complete_dict)
		influx_client.write_points(json_body)

influx_connection_string=os.getenv('INFLUX_INFO','influxdb://root:root@influx:8086/batmon')
mqtt_username=os.getenv('MQTT_USERNAME','charm')
mqtt_password=os.getenv('MQTT_PASSWORD','l0ra1sCharm1ng')

influx_client = InfluxDBClient.from_dsn(influx_connection_string, timeout=5)
client = mqtt.Client()
client.username_pw_set(mqtt_username, mqtt_password)

client.on_connect = on_connect
client.on_message = on_message
client.connect("lora-ttn-ns", 1883, 60)


client.subscribe("cern-charm-2/devices/#", 2)
client.loop_forever()
